const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const exphbs = require('express-handlebars');
const homeRoutes = require('./routes/home');
const cardRoutes = require('./routes/card');
const coursesRoutes = require('./routes/courses');
const addRoutes = require('./routes/add');

const app = express();

const hbs = exphbs.create({
	defaultLayout: 'main',
	extname: 'hbs'
})

app.engine('hbs', hbs.engine)
app.set('view engine', 'hbs')
app.set('views', 'views')

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({ extended: true }))

app.use('/', homeRoutes);
app.use('/add', addRoutes);
app.use('/courses', coursesRoutes);
app.use('/card', cardRoutes);

const port = process.env.port || 4000

async function start() {
	try {
		const url = `mongodb+srv://stepan:3ShypHHRjpES9fzq@cluster0-4ohuw.mongodb.net/shop`;
		await mongoose.connect(url, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useFindAndModify: false
		})
		app.listen(port, () => {
			console.log(`Server is running on port ${port}...`);
		})
	} catch (error) {
		console.log(error)
	}
}

start()

//const password = '3ShypHHRjpES9fzq'
//const myIP = '78.30.19.199/32'
